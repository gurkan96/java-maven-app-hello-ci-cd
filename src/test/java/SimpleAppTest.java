import static org.junit.Assert.assertEquals;
import org.junit.Test;
import spark.Request;
import spark.Response;

public class SimpleAppTest {

    @Test
    public void testHelloWorldRoute() {
        // Arrange
        Request request = null; // You can create mock requests if needed
        Response response = null; // You can create mock responses if needed

        // Act
        String result = SimpleApp.helloWorldRoute(request, response);

        // Assert
        assertEquals("Hello, World!", result);
    }

    // Add more tests as needed
}