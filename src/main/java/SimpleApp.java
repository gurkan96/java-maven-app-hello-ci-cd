import static spark.Spark.*;

public class SimpleApp {

    public static void main(String[] args) {
        // Set the port to 6060
        port(6060);

        // Define a route to display "Hello, World!" on the browser
        get("/", SimpleApp::helloWorldRoute);
    }

    // Extracted method for testing
    public static String helloWorldRoute(spark.Request req, spark.Response res) {
        return "Hello, World!";
    }
}