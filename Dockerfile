FROM openjdk:8-jre-alpine

EXPOSE 6060

WORKDIR /home/app

COPY ./target/SimpleJava*.jar /home/app/

ENTRYPOINT ["java", "-jar", "SimpleJavaApp-1.0-SNAPSHOT.jar"]
